import org.junit.Test;
import static org.junit.Assert.assertEquals;
public class TestFactory {
    @Test
    public void testFactory() {
        ChocolateSweets chocolateSweet = new ChocolateSweets("abc", 20);
        JellySweets jellySweets = new JellySweets("cde", 10);
        Lollipop lollipop = new Lollipop("def", 5);
        assertEquals("Party of chocolates", Factory.sort(chocolateSweet));
        assertEquals("Party of jelly sweets", Factory.sort(jellySweets));
        assertEquals("Party of lollipop", Factory.sort(lollipop));
    }
}
