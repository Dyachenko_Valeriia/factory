public interface Sweets {
    int getWeight(); // взвесить

    void packUp();  // упаковать

    boolean packaging(); //проверить, упаковано ли

    String getTitle(); // получить название

    Composition getCompos();
}
