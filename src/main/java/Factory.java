public class Factory {
    public static String sort(Sweets sweets){
        String result = null;
        if(sweets.getCompos().equals(Composition.CHOCOLATE)){
            result = "Party of chocolates";
        }
        if(sweets.getCompos().equals(Composition.JELLY)){
            result = "Party of jelly sweets";
        }
        if(sweets.getCompos().equals(Composition.MOLASSES)){
            result = "Party of lollipop";
        }
        return result;
    }
}
