public class JellySweets implements Sweets {
    private String title;
    private int weight;
    private boolean packing;
    private Composition comp;

    public JellySweets(String title, int weight) {
        this.title = title;
        this.weight = weight;
        this.packing = false;
        this.comp = Composition.JELLY;
    }

    public int getWeight() {
        return weight;
    }

    public void packUp() {
        if (!packaging()) {
            this.packing = true;
            this.weight += 10;
        }
    }

    public boolean packaging() {
        return packing;
    }

    public String getTitle() {
        return title;
    }

    public Composition getCompos() {
        return comp;
    }

    public Composition getComp() {
        return comp;
    }
}
